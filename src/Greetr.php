<?php

namespace Simplexi;

class Greetr
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
    
    public function getDateNow(string $sFormat = null) : string
    {
        \date_default_timezone_set('Asia/Manila');
        return (\is_null($sFormat) === true) ? \date('Y-m-d H:i:s') : \date($sFormat);
    }
}
